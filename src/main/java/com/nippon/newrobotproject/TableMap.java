/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.newrobotproject;

/**
 *
 * @author Nippon
 */
public class TableMap {

    private int width;
    private int height;
    private char symbol;
    private Bomb bomb;
    private Robot robot;
    private Obj[] objects = new Obj[20];
    int objcount = 0;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void addObj(Obj obj) {
        objects[objcount] = obj;
        objcount++;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
        addObj(robot);
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
        addObj(bomb);
    }

    public void printSymbolOn(int x, int y) {
        char symbol = '-';
        for (int o = 0; o < objcount; o++) {
            if (objects[o].isOn(x, y)) {
                symbol = objects[o].getSymbol();

            }
        }
        System.out.print(symbol);
        
    }

    public void showMap() {
        showTitle();
        System.out.println(robot);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                printSymbolOn(x, y);
               
            }
            showNewLine();
        }
    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showcell() {
        System.out.print("-");
    }

    private void showObj(Obj obj) {
        System.out.println(obj.getSymbol());
    }

    private void showBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        return (x >= 0 && x <= (width - 1)) && (y >= 0 && y <= (height - 1));
        //return true;
    }

    public boolean isBomb(int x, int y) {
        return bomb.getx() == x && bomb.gety() == y;
        //return true;
    }
    public boolean isTree(int x , int y){
          for (int o = 0; o < objcount; o++) {
            if (objects[o] instanceof Tree && objects[o].isOn(x, y)) {
                return true;
    }
}
        return false;
    }
    public int fillFuel(int x , int y){
          for (int o = 0; o < objcount; o++) {
            if (objects[o] instanceof Fuel && objects[o].isOn(x, y)) {
                Fuel fuel = (Fuel) objects[o];
                return fuel.fillFuel();
    }
}
        return 0;
    }
}