/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.newrobotproject;

/**
 *
 * @author Nippon
 */
public abstract class Obj {
    protected int x;
    protected int y;
    protected char symbol ;

    public Obj(char symbol,int x, int y) {
        this.x = x;
        this.y = y;
        this.symbol =symbol;
    }

    public int getx() {
        return x;
    }

    public int gety() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

    @Override
    public String toString() {
        return "Obj{" + "x=" + x + ", y=" + y + '}';
    }
    
}
