/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.newrobotproject;

import java.util.Scanner;

/**
 *
 * @author Nippon
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'X', map, 10);
        Bomb bomb = new Bomb(5, 5);
        map.addObj(new Tree(10, 10)); //Priority low
        map.addObj(new Tree(9, 10));
        map.addObj(new Tree(10, 9));
        map.addObj(new Tree(11, 10));
        map.addObj(new Tree(5, 10));
        map.addObj(new Tree(15, 15)); //Priority low
        map.addObj(new Tree(9, 15));
        map.addObj(new Tree(15, 9));
        map.addObj(new Tree(11, 15));
        map.addObj(new Tree(5, 10));
        map.addObj(new Fuel(0, 5, 20));
        map.addObj(new Fuel(15, 15, 20));
        map.addObj(new Fuel(17, 6, 20));
   
        map.setBomb(bomb);
        map.setRobot(robot); //Priority high
        while (true) {
            map.showMap();
            //W,a | N,w | E,d | S,s | q : quit
            char direction = inputDirection(kb);
            if (direction == 'q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printByeBye() {
        System.out.println("Bye Bye");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        return str.charAt(0);

    }
}
